DevArticlator
=============

This project provides the ability to pull down all of your Dev.to articles.
The objective is to be able to push your articles from a git repository as
outlined in the Dev.to article:
https://dev.to/jessekphillips/hobby-project-dev-to-api-59ln


Usage
-----
The program utilizes a Dev.to API key stored in an environment variable
`appkey`. At this time there are no commandline arguments as the application
will just pull your articles.

```
export apikey=<https://dev.to/settings/account>
./devarticulator
```

After execution you will find all of your articles and meta data in
'devArticles'.
