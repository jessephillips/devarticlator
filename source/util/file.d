/**
 * Helper methods for utilizing file operations.
 */
module util.file;

import std.typecons;

import iopipe.valve;
import iopipe.textpipe;
import iopipe.bufpipe;
import std.io;

/*
 * Creates an iopipe output range for characters to the filename specified.
 */
@trusted
auto fileWriter(string filename) {
    return bufd!char
        .push!(p => p
            .encodeText
            .outputPipe(std.io.File(filename, mode!"w").refCounted))
        .textOutput;
}
