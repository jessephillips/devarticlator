module unittesting.articlegenerator;

import std.datetime;
import std.random;
import std.range;

import vibe.data.json;

import devto.api;

@safe:

string articleTemplate = `{
    "published_at":null,
    "canonical_url":"https://dev.to/username/articleslug",
    "comments_count":0,
    "page_views_count":0,
    "id":111111,
    "published_timestamp":"2020-01-16T01:22:00Z",
    "positive_reactions_count":0,
    "published":false,
    "body_markdown":"Text used in the article.",
    "title":"Title of the Article",
    "url":"https://dev.to/username/articleslug",
    "user":{
      "username":"username",
      "profile_image":"https://res.cloudinary.com/practicaldev/SomeImage.png",
      "github_username":"GitHubUserName",
      "profile_image_90":"https://res.cloudinary.com/practicaldev/SomeImage.png",
      "website_url":"https://www.example.com",
      "twitter_username":null,
      "name":"User Name"
    },
    "cover_image":null,
    "slug":"articleslug",
    "description":"",
    "path":"/username/articleslug",
    "tag_list":[
      "testing"
    ],
    "type_of":"article"
  }`;

/**
 * Provides an initial article template for testing.
 *
 * It is expected to be combined with std.range.generate and seqence.
 *
 * `generate!fakeArticleMe.seqence().take(3);`
 */
auto fakeArticleMe() {
    // Deep copy Json object
    return articleTemplate.parseJsonString;
}

/**
 * Given a range of `fakeArticleMe()` apply a decending id and publish date
 * to replicate the behavior of the API.
 *
 * Note: This does not provide any unpublished articles which are provided
 * first in the API.
 */
auto seqence(R)(R articles, const SysTime newestDate = Clock.currTime)
            if(is(ElementType!R == Json)) {
    struct DateSequencing {
        DateTime date;
        typeof(iota(3).randomCover) id;
        R inner;

        this(typeof(id) ids) {
            id = ids;
        }

        auto empty () {
            if(inner.empty) return true;
            if(id.empty) return true;
            return false;
        }

        auto front() {
            auto ans = inner.front;
            ans["id"] = Json(id.front);
            // DateTime has no timezone, we pretend it is UTC after cast
            ans["published_timestamp"] = Json(date.toISOExtString ~ "Z");
            ans["published"] = Json(true);
            return ans;
        }

        auto popFront() {
            inner.popFront();
            id.popFront;
            date -= dur!"days"(uniform(1,12));
        }
    }

    DateSequencing ds = DateSequencing(iota(10_000).randomCover);
    ds.date = cast(DateTime) newestDate;
    ds.inner = articles;
    return ds;
} unittest {
    import std.algorithm : equal, map, isSorted;
    auto seq = generate!fakeArticleMe.seqence(Clock.currTime).take(3);
    assert(seq.map!(x => x["published_timestamp"].get!string).array.isSorted!"a > b");
}
