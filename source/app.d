import vibe.core.log;
import vibe.http.client;
import vibe.data.json;

import std.process;
import std.algorithm;
import std.range;

import devto.api;
import devtopull;

@safe:

void main()
{
    auto savePath = "devArticles";
    auto pullStateFile = "pullState.json";

    import articlewriter;
    import std.file;
    import std.path;
    if(!exists(buildPath(savePath, articleContentFolder)))
        mkdirRecurse(buildPath(savePath, articleContentFolder));

    PullState ps;
    if(exists(pullStateFile))
        ps = readLastPull(pullStateFile);

    alias inPreviousPull = (x) => !x.isNewerArticle(ps);

    auto newestArt = devtoMyArticles()
        .until!inPreviousPull
        .structureArticles
        .tee!(x => x.saveArticle(savePath))
        // Don't use unpublished articles to determine newer
        .filter!(x => x.meta.published)
        .reduceToNewest;

    import std.datetime;
    ps.lastArticle = newestArt.meta.id;
    ps.datePulled = Clock.currTime;

    ps.saveLastPull(pullStateFile);
}
