/**
 * Handle meta information related to pulling articles from Dev.to
 */
module devtopull;

import std.datetime;
import std.range;

import vibe.data.json;

import devto.api;

version(unittest) {
    import unittesting.articlegenerator;
    import std.algorithm : map, until, equal, fold;
}

/**
 * Provides information about the last execution to pull.
 *
 * This structure will allow retrieving newer articles since the last session.
 */
struct PullState {
    /// When the pull was executed
    SysTime datePulled;
    /// The newest article ID obtained during the pulling
    uint lastArticle;
}

/**
 * Determine if an article is newer than the last PullState.
 */
@safe pure nothrow @nogc
auto isNewerArticle(const ArticleMe am, const PullState ps) {
    if(!am.published) return true;
    return am.published_timestamp > ps.datePulled;
} unittest {
    PullState ps;
    ps.datePulled = SysTime(DateTime(2018, 1, 1, 10, 30, 0), UTC());
    alias inPreviousPull = (x) => !x.isNewerArticle(ps);
    auto devreq = generate!fakeArticleMe
        .seqence(ps.datePulled)
        .map!(x => x.deserializeJson!(ArticleMe))
        .take(3);
    assert(devreq.until!inPreviousPull.empty);
} unittest {
    PullState ps;
    ps.datePulled = SysTime(DateTime(2018, 1, 1, 10, 30, 0), UTC());
    alias inPreviousPull = (x) => !x.isNewerArticle(ps);
    auto devreq = generate!fakeArticleMe
        .seqence(ps.datePulled + dur!"days"(1))
        .map!(x => x.deserializeJson!(ArticleMe))
        .take(3)
        .map!(x => cast(immutable(ArticleMe))x);
    assert(devreq.until!inPreviousPull.map!(x => x.published_timestamp).equal([ps.datePulled + dur!"days"(1)]));
}

/**
 * Deserializes file into the PullState structure
 */
@safe
PullState readLastPull(string filename) {
    import std.file : readText;
    return deserializeJson!(PullState)(readText(filename));
}

/**
 * Serializes PullState structure into file
 */
@trusted
void saveLastPull(const PullState ps, string filename) {
    import util.file : fileWriter;
    import std.algorithm : copy;
    serializeToPrettyJson(ps).copy(fileWriter(filename));
}

struct DevToRange {
    private const(ArticleMe)[] function(uint page) @safe articlePage;

    const(ArticleMe)[] data;
    uint page = 1;

    @safe nothrow pure const
    const(ArticleMe) front() {
        return data.front;
    }

    @safe nothrow pure const
    bool empty() {
        return data.empty;
    }

    @safe
    void popFront() {
        data.popFront;
        if(data.empty)
            data = articlePage(++page);
    }
} unittest {
    alias fakeArticles = (uint page) @safe {
        return () @trusted {
        static iteration = 0;
        if(iteration++ < 1)
        return generate!fakeArticleMe
            .map!(x => cast(immutable(ArticleMe))x.deserializeJson!(ArticleMe))
            .take(3)
            .array;
        return generate!fakeArticleMe
            .map!(x => cast(immutable(ArticleMe))x.deserializeJson!(ArticleMe))
            .take(0)
            .array;
        }();
    };

    DevToRange dtv;
    dtv.articlePage = fakeArticles;
    dtv.data = dtv.articlePage(1);

    import std.algorithm : count;
    assert(dtv.count == 3);
} unittest {
    alias fakeArticles = (uint page) @safe {
        return () @trusted {
        static iteration = 0;
        if(iteration++ < 2)
        return generate!fakeArticleMe
            .map!(x => cast(immutable(ArticleMe))x.deserializeJson!(ArticleMe))
            .take(3)
            .array;
        return generate!fakeArticleMe
            .map!(x => cast(immutable(ArticleMe))x.deserializeJson!(ArticleMe))
            .take(0)
            .array;
        }();
    };

    DevToRange dtv;
    dtv.articlePage = fakeArticles;
    dtv.data = dtv.articlePage(1);

    import std.algorithm : count;
    assert(dtv.count == 6);
}

@safe
DevToRange devtoMyArticles() {
    DevToRange dtv;
    dtv.articlePage = &devArticles;
    dtv.data = dtv.articlePage(1);
    return dtv;
}

@safe
const(ArticleMe)[] devArticles(uint page) {
    import vibe.core.log;
    import vibe.http.client;
    import std.process;
    import std.format;

    const(ArticleMe)[] ret;

    requestHTTP(format!"https://dev.to/api/articles/me/all?page=%s"(page),
        (scope req) @safe {
            req.headers.addField("api-key", environment["appkey"]);
        },
        (scope res) @safe {
            if(res.statusCode != 200)
                return;
                import std.algorithm : each;
            auto data = res.readJson().get!(Json[]);
            data.each!(x => x["published_timestamp"] = x["published_timestamp"].get!string.empty ?  SysTime.init.toISOExtString : x["published_timestamp"].get!string);
            ret = meArticles(Json(data));
        }
    );

    return ret;
}
