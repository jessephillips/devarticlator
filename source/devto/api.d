/**
 * This module provides had written structures for https://docs.dev.to/api/ and additional
 * parsing methods.
 *
 * The structures may not reflect the complete data returned by the API as this will help
 * on the items important to this application.
 */
module devto.api;

import std.datetime;

import vibe.data.serialization;
import vibe.data.json;

/**
 * Article structure returned when getting articles from the Me endpoint.
 */
struct ArticleMe {
    @optional string type_of;
    @optional int id;
    @optional string title;
    @optional string description;
    @optional string cover_image;
    @optional bool published;
    @optional SysTime published_timestamp;
    @optional string[] tag_list;
    @optional string canonical_url;
    @optional string body_markdown;
    @optional string slug;
}

/**
 * Article Structure defining data for creating an article on the Create endpoint.
 */
struct ArticleCreate {
    string title;
    @optional string body_markdown;
    @optional bool published;
    @optional string series;
    @optional string main_image;
    @optional string canonical_url;
    @optional string description;
    @optional string[] tags;
    @optional int organization_id;
}

/**
 * Deserialize a Json Blob into an array of `ArticleMe`.
 */
@safe
auto meArticles(const Json resData) {
    return deserializeJson!(const(ArticleMe)[])(resData);
} unittest {
    auto exampleResponse = `[
  {
    "published_at":null,
    "canonical_url":"http://example.com/",
    "comments_count":0,
    "page_views_count":0,
    "id":12345,
    "published":false,
    "body_markdown":"Content Body",
    "title":"Article Title",
    "description":"",
    "tag_list":[
      "testing"
    ],
    "type_of":"article"
  }]`.parseJsonString;

    ArticleMe ans;
    ans.type_of = "article";
    ans.id = 12345;
    ans.title = "Article Title";
    ans.description = "";
    ans.cover_image = "";
    ans.published = false;
    ans.tag_list = ["testing"];
    ans.canonical_url = "http://example.com/";
    ans.body_markdown = "Content Body";

    import std.algorithm;
    import std.conv;

    assert(meArticles(exampleResponse).equal([ans]), meArticles(exampleResponse).to!string);
}
