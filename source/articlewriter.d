/**
 * This module focuses on methods for writing articles to disk.
 *
 * Article storage is broken into two parts, meta data and markdown content.
 * A folder location may be specified but markdown content will always be stored
 * in a sub folder `articleContentFolder`.
 */
module articlewriter;

import vibe.core.log;
import vibe.data.json;

import std;

import devto.api;

version(unittest) {
    import unittesting.articlegenerator;
    import std.algorithm : map, until, equal, fold;
    import devtopull;
}

/**
 * This is the structure used to store article meta data as Json to disk.
 *
 * This content is pimarily defined from the devto API, where `content_file`
 * is a separate field used to define the relative filesystem location of
 * the markdown file to the meta file.
 */
struct ArticleMeta {
    @optional string title;
    @optional bool published;
    @optional int id;
    @optional int organization_id;
    @optional string canonical_url;
    @optional string description;
    @optional string main_image; // Cover image
    @optional string series;
    @optional string type_of;
    @optional const(string)[] tags;
    @optional string content_file;
    @optional string slug;

    /**
     * Construct a Meta article from `ArticleMe`.
     *
     * Useful when processing existing articles from the devto API.
     */
    @safe pure nothrow @nogc
    this(const ArticleMe me) {
        canonical_url = me.canonical_url;
        description = me.description;
        id = me.id;
        main_image = me.cover_image;
        published = me.published;
        series = "";
        tags = me.tag_list;
        title = me.title;
        type_of = me.type_of;
        slug = me.slug;
    }

    /**
     * Convert Meta data into `ArticleCreate`.
     *
     * Useful to call devto api to create an article.
     *
     * This does not retrieve the `content_file` from disk, it will be require
     * to add the `body_markdown` after making this conversion.
     */
    @safe pure nothrow @nogc
    T opCast(T : ArticleCreate)() {
        ArticleCreate ac;
        ac.title = title;
        ac.published = published;
        ac.series = series;
        ac.main_image = main_image;
        ac.canonical_url = canonical_url;
        ac.description = description;
        ac.tags = tags;
        ac.organization_id = organization_id;
        ac.slug = slug;
        return ac;
    }
}

/// This defines the sub folder for storing articles.
enum articleContentFolder = "Articles";

/**
 * Separate the Meta data and Content for easier disk writing and retrieval.
 */
alias Article = Tuple!(ArticleMeta, "meta", string, "content");

/**
 * Given a range of `ArticleMe` separate the data into meta data and content.
 *
 * Returns: Range of `Article`
 */
@safe pure nothrow
auto structureArticles(Range)(Range articles) if (isInputRange!Range
                               && is(Unqual!(ElementType!Range) == ArticleMe)) {
    return articles.map!(x => Article(ArticleMeta(x), x.body_markdown));
} unittest {
    auto exampleResponse = `[
  {
    "published_at":null,
    "canonical_url":"http://example.com/",
    "comments_count":0,
    "page_views_count":0,
    "id":12345,
    "published":false,
    "body_markdown":"Content Body",
    "title":"Article Title",
    "description":"",
    "tag_list":[
      "testing"
    ],
    "type_of":"article",
    "slug":"slugger"
  }]`.parseJsonString.meArticles;

    ArticleMeta ans;
    ans.type_of = "article";
    ans.id = 12345;
    ans.title = "Article Title";
    ans.description = "";
    ans.main_image = "";
    ans.published = false;
    ans.tags = ["testing"];
    ans.canonical_url = "http://example.com/";
    ans.series = "";
    ans.slug = "slugger";

    import std.algorithm;
    import std.conv;

    assert(structureArticles(exampleResponse).equal([Article(ans, "Content Body")]));
}

/**
 * Meta information about pathing for article content and meta data.
 */
private struct Paths {
    /// Location article content/markdown is stored.
    string articleSaveLocation;
    /// Location meta data is stored.
    string metaSaveLocation;
    /// Article content location relative to metaSaveLocation,
    /// stored in meta data.
    string articleMetaPath;
}

/*
 * Creates a path relationship for an article.
 */
@safe pure nothrow
private const(Paths) articlePaths(string folder, const ArticleMeta am)
out(path) {
    assert(path.articleSaveLocation != path.metaSaveLocation, "Can't save to same file");
    assert(path.articleSaveLocation.endsWith(path.articleMetaPath), "Meta Reference to Article File must match.");
    assert(!path.articleSaveLocation.find(folder).empty, "Save Locations should utilized specified folder");
    assert(!path.metaSaveLocation.find(folder).empty, "Save Locations should utilized specified folder");
} do {
    Paths ret;

    auto filename = am.slug;
    assert(isValidFilename(filename), "Article Name Conflicts with valid file name.");

    ret.articleMetaPath = buildPath(articleContentFolder, filename.setExtension("md"));
    ret.articleSaveLocation = folder.buildPath(ret.articleMetaPath);
    ret.metaSaveLocation = folder.buildPath(filename.setExtension("devto"));

    return ret;
} unittest {
    ArticleMeta meta;
    meta.title = "Article Title";
    meta.slug = "article_title_oul3o";
    auto suppressWarning = articlePaths("someLocation", meta);
}

/**
 * Writes articles to specified folder.
 *
 * Article content will be stored in `articleContentFolder` as a sub folder.
 *
 * Params:
 *     articles = Build from call to `structureArticles()`
 *     folder   = Filesystem folder location to place articles
 *
 * See_Also:
 *     structureArticles, Article, articleContentFolder
 */
@trusted
void saveArticle(Article article, string folder) {
    import util.file : fileWriter;
    import std.algorithm : copy;
    auto paths = articlePaths(folder, article.meta);

    article.meta.content_file = paths.articleMetaPath;
    article.content.copy(fileWriter(paths.articleSaveLocation));
    article.meta.serializeToPrettyJson.copy(fileWriter(paths.metaSaveLocation));
}

/**
 * Reduces the range into the newest article.
 *
 * Combine this with std.range.tee to write out articles as
 * the latest article is being reduced.
 */
@safe
auto reduceToNewest(Range)(Range articles)
                                        if(is(ElementType!Range == Article)) {
    return articles.fold!((a, b) => a.meta.id < b.meta.id ? b : a);
} unittest {
    PullState ps;
    ps.datePulled = SysTime(DateTime(2018, 1, 1, 10, 30, 0), UTC());
    alias inPreviousPull = (x) => !x.isNewerArticle(ps);
    auto devreq = generate!fakeArticleMe
        .seqence(ps.datePulled + dur!"days"(1))
        .map!(x => x.deserializeJson!(ArticleMe))
        .take(3);
    auto newestid = devreq.front.id;
    assert(devreq.until!inPreviousPull
           .structureArticles
           .reduceToNewest
           .meta.id == newestid);
}
